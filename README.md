# **How to browser renders a webpage and mechanism behind it.**

* How the broweser render a webpage. The browser blocks some rendering of a webpage until cartain resources are loaded first while other resources are loaded asynchronausly.
### **Before the understanding browser rendering we learn this topics:**

1. Document object Method (DOM)
2. CSS object Method (CSSOM)
3. Render Tree

## Document object method (DOM) :
* The Dom is the structured representation of the HTML document created by browser.
* It allows Javascript to Manipulate structure and style your website 

## HTML Example

```HTML
<!doctype html>
<html>
    <head></head>
        <body>
          <div class="bg-card">
            <div class="introduction">
                <h1>Hello World!</h1>
                <p>Hii This is Madhu</p>
                <p id="root"></p>
            </div>
          </div>
          <script >
            let rootEl = document.getElementById("root")
            rootEl.textContent = "I'm a software developer"
          </script>
        </body>
</html>
```

* In Javascript code the **Document** is a object.
* It isthe entry point of **DOM** for accising any HTML element, you should always start with accessing the document object first.
* Document object used to create Element, add styles, add content what we need...
* Using DOM we can create websites Dynamically.
* But here the DOM is not Javascript specification, it is created by the **Web APIs**

# HTML DOM Tree structure
* The DOM tree represents an HTML document as [Nodes](https://developer.mozilla.org/en-US/docs/Web/API/Node). Each node is reffered to as an object.
* The browser stores the HTML Document as **Tree Structure representation** 
* It used for change structure, styles, content in HTML using Javascript 



![HTML Image](https://www.w3schools.com/js/pic_htmltree.gif)

# Cascading style sheet (CSS)
* When we design a website our intentions are to make it as good locking. 
* We do that by providing some styles to HTML elements.
* For the providing styles we use cascading style sheets(CSS).
* using css selectors, we can target DOM elements and set a values to style propertys such as color and font-size etc..

## CSS
```css
.bg-card {
    height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}

.introduction {
    background-color: blue;
    height: 300px;
    width: 560px;
    text-align: center;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}

h1 {
    color: #ffffff;
}

p {
    color: #ffffff;
}
```

# CSS Object Method (CSSOM)
* After constructing the DOM the Browser reads css from all the sources(External,Embedded,inline) and construct a CSSOM.
* Which is a Tree like structare just like DOM 

![CSS Image](https://i.stack.imgur.com/l9uwf.png)

# Render Tree 

* When a web page loaded, the browser reads the HTML file as **DOM** tree Structure. 
* after the browser reads cssfile as **CSSOM** tree Structare.
* The browser combion the **DOM** and **CSSOM** into a **Render Tree**

![render tree Image](https://miro.medium.com/max/1838/1*3Imspi9H9gdXg92ofdR7wQ.png)


## Finally the browser run layout on the render tree complete geometry of each node. and print the individual nodes to the screen 

**After browser rendering the output like this**

![output Image](https://res.cloudinary.com/dqcchr4gn/image/upload/v1659810230/Screenshot_from_2022-08-06_23-51-33_vub6di.png)

## References Sessions and Website 
* [How a web browser builds and displays a web page](https://www.youtube.com/watch?v=DuSURHrZG6I)
* [How the browser renders a web page?](https://medium.com/jspoint/how-the-browser-renders-a-web-page-dom-cssom-and-rendering-df10531c9969) 
